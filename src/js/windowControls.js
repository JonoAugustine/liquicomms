const { remote, ipcRenderer } = require("electron");

const getCurrentWindow = () => remote.getCurrentWindow();

const openMenu = (x, y) => {
  ipcRenderer.send(`display-app-menu`, { x, y });
};

const minimizeWindow = (browserWindow = getCurrentWindow()) => {
  if (browserWindow.minimizable) {
    browserWindow.minimize();
  }
};

const maximizeWindow = (browserWindow = getCurrentWindow()) => {
  if (browserWindow.maximizable) {
    browserWindow.maximize();
  }
};

function unmaximizeWindow(browserWindow = getCurrentWindow()) {
  browserWindow.unmaximize();
}

function maxUnmaxWindow(browserWindow = getCurrentWindow()) {
  if (browserWindow.isMaximized()) {
    browserWindow.unmaximize();
  } else {
    browserWindow.maximize();
  }
}

const closeWindow = (browserWindow = getCurrentWindow()) => {
  browserWindow.close();
};

const isWindowMaximized = (browserWindow = getCurrentWindow()) => {
  return browserWindow.isMaximized();
};

module.exports = {
  getCurrentWindow,
  openMenu,
  minimizeWindow,
  maximizeWindow,
  unmaximizeWindow,
  maxUnmaxWindow,
  isWindowMaximized,
  closeWindow,
};
