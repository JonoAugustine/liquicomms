const firebase = require("firebase/app");
require("firebase/auth");
require("firebase/firestore");
require("firebase/functions");

const api = firebase.initializeApp({
  apiKey: "AIzaSyC575lOrrlqXASBl5Podp9KmVoUJ2lueqk",
  authDomain: "liquicomms.firebaseapp.com",
  databaseURL: "https://liquicomms.firebaseio.com",
  projectId: "liquicomms",
  storageBucket: "liquicomms.appspot.com",
  messagingSenderId: "614846412286",
  appId: "1:614846412286:web:8ee66a45188e3ad4183caf",
  measurementId: "G-4RN5QLQ0ZH",
});

const auth = api.auth();
const functions = api.functions();

module.exports = { auth, functions };
